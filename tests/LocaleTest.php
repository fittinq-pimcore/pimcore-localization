<?php declare(strict_types=1);

namespace Tests\Fittinq\Pimcore\Localization;

use Fittinq\Pimcore\Localization\Exception\InvalidLocaleException;
use Fittinq\Pimcore\Localization\Locale\Locale;
use PHPUnit\Framework\TestCase;

class LocaleTest extends TestCase
{
    public function test_throwExceptionLocaleIsUnknown()
    {
        $this->expectException(InvalidLocaleException::class);

        new Locale('xx_XX');
    }

    /**
     * @dataProvider getLocales
     */
    public function test_getValuesFromLocaleIfValid(string $locale, string $language)
    {
        $actual = new Locale($locale);
        $this->assertEquals($locale, $actual->getLocale());
        $this->assertEquals($language, $actual->getLanguage());
    }

    public static function getLocales(): array
    {
        return [
            ['nl_NL', 'nl'],
            ['de_DE', 'de'],
            ['en_GB', 'en']
        ];
    }
}
