<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Localization\Locale;

use Exception;
use Fittinq\Pimcore\Localization\Exception\InvalidLocaleException;
use Pimcore\Tool;

class Locale
{
    private string $locale;
    private string $language;

    /**
     * @throws Exception
     */
    public function __construct(string $locale)
    {
        if (!in_array($locale, Tool::getValidLanguages())) {
            throw new InvalidLocaleException();
        }

        $this->locale = $locale;
        $parts = explode('_', $locale);
        $this->language = $parts[0];
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function getLanguage(): string
    {
        return $this->language;
    }
}
